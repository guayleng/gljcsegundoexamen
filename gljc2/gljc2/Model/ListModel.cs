﻿using gljc2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gljc2.Model
{
    [Serializable]
    public class ListModel : IToFile, IFromFile<ListModel>
    {
        public string ListName { get; set; }
        public string ItemName { get; set; }

        public ListModel FromXml(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToXml(string filepath)
        {
            throw new NotImplementedException();
        }
    }
}
